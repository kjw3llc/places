<?php if(!isset($_POST['type'])) $_POST['type'] = 'bar'; ?>

<!DOCTYPE html>
<html>
    <head>
        <title>Place searches</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        <link href="default.css" rel="stylesheet">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true&libraries=places"></script>
        <script>
            var apiKey = "AIzaSyDPlAyo5gqV2o5RSXE780yy8u76azRxago";
            var geocoder = new google.maps.Geocoder();
            var loc;
            var map;
            var service;
            var infowindow;
            var milesToMeters = 1609.34;
            var searchRadiusM = 5 * milesToMeters;
            var searchTypes = ['bar'];
            var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

            function initialize(loc) {
                map = new google.maps.Map(document.getElementById('map-canvas'), {
                    mapTypeId : google.maps.MapTypeId.ROADMAP,
                    center : loc,
                    zoom : 11
                });

                var request = {
                    key : apiKey,
                    location : loc,
                    radius : searchRadiusM,
                    types : searchTypes,
                    rankby : google.maps.places.RankBy.PROMINENCE,
                    sensor : false
                };
                infowindow = new google.maps.InfoWindow();
                service = new google.maps.places.PlacesService(map);
                service.nearbySearch(request, callback);
            }

            function callback(results, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    $("#place-listing").html("");
                    for (var i = 0; i < results.length; i++) {
                        createMarker(results[i]);
                    }
                }
            }

            function createMarker(place) {
                var placeLoc = place.geometry.location;
                var marker = new google.maps.Marker({
                    map : map,
                    position : place.geometry.location
                });

                var request = {
                    key : apiKey,
                    reference : place.reference,
                    sensor : false
                }
                service.getDetails(request, function(details, status) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        var detailsHtml = '<section class="place">';

                        var formattedPhoneHtml = '';
                        if (details.formatted_phone_number != undefined)
                            formattedPhoneHtml = '<div style="float:right;">' + details.formatted_phone_number + '</div>';

                        detailsHtml += '<h3>' + details.name + formattedPhoneHtml + '</h3>';
                        detailsHtml += '<div>' + details.formatted_address + '</div>';
                        if (details.website != undefined)
                            detailsHtml += '<div><a href="' + details.website + '">' + details.website + '</a></div>';

                        detailsHtml += prettifyHours(details.opening_hours);

                        detailsHtml += '</section>';

                        $("#place-listing").append(detailsHtml);
                    } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                        setTimeout(function() {
                            createMarker(place);
                        }, 500);
                    }
                });

                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(place.name);
                    infowindow.open(map, this);

                });
            }

            function prettifyHours(periods) {
                var hoursHtml = "";
                var hours = 0;

                if (periods != undefined) {
                    console.log(periods);

                    hoursHtml += '<h4>Operating Hours: ' + (periods.open_now ? '<span style="color:green;">OPEN</span>' : '<span style="color:red;">CLOSED</span>') + '</h4><ul>';
                    for ( i = 0; i < periods.periods.length; i++) {
                        hoursHtml += '<li>' + days[i] + ': ' + convertTime(periods.periods[i].open.hours) + ' to ' + convertTime(periods.periods[i].close.hours) + '</li>';
                        console.log(periods.periods[i]);
                    }
                    hoursHtml += '<ul>';

                } else {
                    return "";
                }

                return hoursHtml;
            }
            
            function convertTime(time) {
                amPm = "am";
                if (time > 12) amPm = "pm" 
                time = ((time + 11) % 12 + 1);

                return time + ' ' + amPm
            }

            function codeAddress(address) {
                geocoder.geocode({
                    'address' : address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        loc = results[0].geometry.location;
                        initialize(loc);
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }

            $(function() {
                searchTypes = [$("#type").val()];
                searchRadiusM = $("#distance").val() * milesToMeters;

                if ($("#location").val().length > 0) {
                    codeAddress($("#location").val());
                } else {
                    codeAddress("Charlottesville, VA");
                }
            });
        </script>
    </head>
    <body>
        <form style="margin:5px;" method="post">
            <input type="text" name="location" id="location" placeholder="City, State or Zip..." <?php if(isset($_POST['location']) && strlen($_POST['location']) > 0) echo 'value="', $_POST['location'], '"'; ?>
            />
            <select name="type" id="type">
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'amusement_park') echo ' selected="selected"'; ?> value="amusement_park">Amusement Park</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'aquarium') echo ' selected="selected"'; ?> value="aquarium">Aquarium</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'art_gallery') echo ' selected="selected"'; ?> value="art_gallery">Art Gallery</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'bakery') echo ' selected="selected"'; ?> value="bakery">Bakery</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'bar') echo ' selected="selected"'; ?> value="bar">Bar</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'beauty_salon') echo ' selected="selected"'; ?> value="beauty_salon">Beauty Salon</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'bicycle_store') echo ' selected="selected"'; ?> value="bicycle_store">Bicycle Store</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'book_store') echo ' selected="selected"'; ?> value="book_store">Book Store</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'bowling_alley') echo ' selected="selected"'; ?> value="bowling_alley">Bowling Alley</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'cafe') echo ' selected="selected"'; ?> value="cafe">Cafe</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'campground') echo ' selected="selected"'; ?> value="campground">Campground</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'casino') echo ' selected="selected"'; ?> value="casino">Casino</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'church') echo ' selected="selected"'; ?> value="church">Church</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'clothing_store') echo ' selected="selected"'; ?> value="clothing_store">Clothing Store</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'establishment') echo ' selected="selected"'; ?> value="establishment">Establishment</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'florist') echo ' selected="selected"'; ?> value="florist">Florist</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'food') echo ' selected="selected"'; ?> value="food">Food</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'gym') echo ' selected="selected"'; ?> value="gym">Gym</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'hair_care') echo ' selected="selected"'; ?> value="hair_care">Hair Care</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'health') echo ' selected="selected"'; ?> value="health">Health</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'hindu_temple') echo ' selected="selected"'; ?> value="hindu_temple">Hindu Temple</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'jewelry_store') echo ' selected="selected"'; ?> value="jewelry_store">Jewelry Store</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'library') echo ' selected="selected"'; ?> value="library">Library</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'liquor_store') echo ' selected="selected"'; ?> value="liquor_store">Liquor Store</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'lodging') echo ' selected="selected"'; ?> value="lodging">Lodging</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'meal_delivery') echo ' selected="selected"'; ?> value="meal_delivery">Meal Delivery</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'meal_takeaway') echo ' selected="selected"'; ?> value="meal_takeaway">Meal Takeaway</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'mosque') echo ' selected="selected"'; ?> value="mosque">Mosque</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'movie_rental') echo ' selected="selected"'; ?> value="movie_rental">Movie Rental</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'movie_theater') echo ' selected="selected"'; ?> value="movie_theater">Movie Theater</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'museum') echo ' selected="selected"'; ?> value="museum">Museum</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'night_club') echo ' selected="selected"'; ?> value="night_club">Night Club</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'park') echo ' selected="selected"'; ?> value="park">Park</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'pet_store') echo ' selected="selected"'; ?> value="pet_store">Pet Store</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'pharmacy') echo ' selected="selected"'; ?> value="pharmacy">Pharmacy</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'place_of_worship') echo ' selected="selected"'; ?> value="place_of_worship">Place of Worship</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'restaurant') echo ' selected="selected"'; ?> value="restaurant">Restaurant</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'shopping_mall') echo ' selected="selected"'; ?> value="shopping_mall">Shopping Mall</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'spa') echo ' selected="selected"'; ?> value="spa">Spa</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'stadium') echo ' selected="selected"'; ?> value="stadium">Stadium</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'synagogue') echo ' selected="selected"'; ?> value="synagogue">Synagogue</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'university') echo ' selected="selected"'; ?> value="university">University</option>
                <option<?php if(isset($_POST['type']) && $_POST['type'] == 'zoo') echo ' selected="selected"'; ?> value="zoo">Zoo</option>
            </select>
            <select name="distance" id="distance">
                <option<?php if(isset($_POST['distance']) && $_POST['distance'] == '5') echo ' selected="selected"'; ?>>5</option>
                <option<?php if(isset($_POST['distance']) && $_POST['distance'] == '10') echo ' selected="selected"'; ?>>10</option>
                <option<?php if(isset($_POST['distance']) && $_POST['distance'] == '15') echo ' selected="selected"'; ?>>15</option>
                <option<?php if(isset($_POST['distance']) && $_POST['distance'] == '20') echo ' selected="selected"'; ?>>20</option>
                <option<?php if(isset($_POST['distance']) && $_POST['distance'] == '30') echo ' selected="selected"'; ?>>30</option>
                <option<?php if(isset($_POST['distance']) && $_POST['distance'] == '40') echo ' selected="selected"'; ?>>40</option>
                <option<?php if(isset($_POST['distance']) && $_POST['distance'] == '50') echo ' selected="selected"'; ?>>50</option>
            </select>
            <button name="search" id="search" class="btn">
                Search
            </button>
        </form>

        <div id="place-listing"></div>
        <div id="map-canvas"></div>
    </body>
</html>
